package me.flyray.crm.core.feignserver;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import me.flyray.common.msg.BizResponseCode;
import me.flyray.crm.core.entity.PersonalBase;
import me.flyray.crm.facade.request.PersonalBaseRequest;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import me.flyray.crm.core.biz.personal.PersonalBaseBiz;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/***
 * 个人客户
 * */
@Api(tags="个人客户")
@Controller
@RequestMapping("feign/personal")
public class FeignPersonal {
	
	@Autowired
	private PersonalBaseBiz personalBaseBiz;
	
	/**
	 * 个人客户查询
	 * @author centerroot
	 * @time 创建时间:2018年9月26日下午4:31:39
	 * @param personalBaseRequest
	 * @return
	 */
	@ApiOperation("个人客户查询")
	@RequestMapping(value = "/queryInfo",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> accountQuery(@RequestBody @Valid PersonalBaseRequest personalBaseRequest){
		Map<String, Object> respMap = new HashMap<>();
		PersonalBase personalBaseReq = new PersonalBase();
		BeanUtils.copyProperties(personalBaseRequest, personalBaseReq);
		PersonalBase personalBase = personalBaseBiz.selectOne(personalBaseReq);
		if (null == personalBase) {
			respMap.put("code", BizResponseCode.PER_NOTEXIST.getCode());
			respMap.put("message", BizResponseCode.PER_NOTEXIST.getMessage());
		}else{
			PersonalBaseRequest personalBaseEntity = new PersonalBaseRequest();
			BeanUtils.copyProperties(personalBase, personalBaseEntity);
			respMap.put("personalBase", personalBaseEntity);
			respMap.put("code", BizResponseCode.OK.getCode());
			respMap.put("message", BizResponseCode.OK.getMessage());
		}
		return respMap;
    }
	
	
}
