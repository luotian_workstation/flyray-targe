package me.flyray.crm.core.biz.personal;

import java.util.List;

import me.flyray.crm.core.entity.PersonalAccountJournal;
import me.flyray.crm.core.mapper.PersonalAccountJournalMapper;
import me.flyray.crm.facade.request.customerAccountJournalRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import me.flyray.common.biz.BaseBiz;
import me.flyray.common.msg.TableResultResponse;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 个人账户流水（充、转、提、退、冻结流水）
 *
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Service
public class PersonalAccountJournalBiz extends BaseBiz<PersonalAccountJournalMapper, PersonalAccountJournal> {
	
	private static final Logger logger= LoggerFactory.getLogger(PersonalAccountJournalBiz.class);

}

