package me.flyray.crm.core.biz.platform;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.flyray.common.msg.BizResponseCode;
import me.flyray.crm.core.entity.PlatformSafetyConfig;
import me.flyray.crm.core.mapper.PlatformSafetyConfigMapper;
import me.flyray.crm.facade.request.SafetyConfigRequest;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import me.flyray.common.biz.BaseBiz;
import me.flyray.common.msg.TableResultResponse;
import me.flyray.common.util.MD5;
import me.flyray.common.util.SHA256Utils;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 平台安全配置信息
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Slf4j
@Service
public class PlatformSafetyConfigBiz extends BaseBiz<PlatformSafetyConfigMapper, PlatformSafetyConfig> {

	/**
	 * 列表
	 * @param bean
	 * @return
	 */
	public TableResultResponse<PlatformSafetyConfig> safetyConfigList(SafetyConfigRequest bean){
		 Example example = new Example(PlatformSafetyConfig.class);
		 Criteria criteria = example.createCriteria();
		 criteria.andEqualTo("platformId",bean.getPlatformId());
		 example.setOrderByClause("create_time desc");
		 Page<PlatformSafetyConfig> result = PageHelper.startPage(bean.getPage(), bean.getLimit());
		 List<PlatformSafetyConfig> list = mapper.selectByExample(example);
		 return new TableResultResponse<PlatformSafetyConfig>(result.getTotal(), list);
	}
	
	/**
	 * 添加
	 * @param bean
	 * @return
	 */
	public Map<String, Object> addConfig(SafetyConfigRequest bean){
		 PlatformSafetyConfig config = new PlatformSafetyConfig();
		 Map<String, Object> respMap = new HashMap<>();
		 try {
			 BeanUtils.copyProperties(config, bean);
			 String appIdCipherText = MD5.md5(SHA256Utils.SHA256(config.getPlatformId()));			 
			 config.setAppId(appIdCipherText);
			 config.setCreateTime(new Date());
			 config.setUpdateTime(new Date());
			 if (mapper.insert(config) > 0) {
				respMap.put("code", BizResponseCode.OK.getCode());
				respMap.put("message", BizResponseCode.OK.getMessage());
				log.info("【添加安全配置】   响应参数：{}", respMap);
				return respMap;
			}
		 } catch (Exception e) {
			e.printStackTrace();
			log.info("【添加安全配置】  报错：" + e.getMessage());
			respMap.put("code", BizResponseCode.SERVICE_NOT_AVALIABLE.getCode());
			respMap.put("message", BizResponseCode.SERVICE_NOT_AVALIABLE.getMessage());
			log.info("【添加安全配置】   响应参数：{}", respMap);
			return respMap;
		 }
		 respMap.put("code", BizResponseCode.SERVICE_NOT_AVALIABLE.getCode());
		 respMap.put("message", BizResponseCode.SERVICE_NOT_AVALIABLE.getMessage());
		 log.info("【添加安全配置】   响应参数：{}", respMap);
		 return respMap;
	}
	
	
	/**
	 * 修改
	 * @param bean
	 * @return
	 */
	public Map<String, Object> updateConfig(SafetyConfigRequest bean){
		 PlatformSafetyConfig config = new PlatformSafetyConfig();
		 Map<String, Object> respMap = new HashMap<>();
		 try {
			 BeanUtils.copyProperties(config, bean);
			 config.setUpdateTime(new Date());
			 if (mapper.updateByPrimaryKeySelective(config) > 0) {
				respMap.put("code", BizResponseCode.OK.getCode());
				respMap.put("message", BizResponseCode.OK.getMessage());
				log.info("【修改安全配置】   响应参数：{}", respMap);
				return respMap;
			}
		 } catch (Exception e) {
			e.printStackTrace();
			log.info("【修改安全配置】  报错：" + e.getMessage());
			respMap.put("code", BizResponseCode.SERVICE_NOT_AVALIABLE.getCode());
			respMap.put("message", BizResponseCode.SERVICE_NOT_AVALIABLE.getMessage());
			log.info("【修改安全配置】   响应参数：{}", respMap);
			return respMap;
		 }
		 respMap.put("code", BizResponseCode.SERVICE_NOT_AVALIABLE.getCode());
		 respMap.put("message", BizResponseCode.SERVICE_NOT_AVALIABLE.getMessage());
		 log.info("【修改安全配置】   响应参数：{}", respMap);
		 return respMap;
	}
	
	
	/**
	 * 删除
	 * @param bean
	 * @return
	 */
	public Map<String, Object> deleteConfig(SafetyConfigRequest bean){
		 PlatformSafetyConfig config = new PlatformSafetyConfig();
		 Map<String, Object> respMap = new HashMap<>();
		 try {
			 BeanUtils.copyProperties(config, bean);
			 if (mapper.delete(config) > 0) {
				respMap.put("code", BizResponseCode.OK.getCode());
				respMap.put("message", BizResponseCode.OK.getMessage());
				log.info("【删除安全配置】   响应参数：{}", respMap);
				return respMap;
			}
		 } catch (Exception e) {
			e.printStackTrace();
			log.info("【删除安全配置】   报错：" + e.getMessage());
			respMap.put("code", BizResponseCode.SERVICE_NOT_AVALIABLE.getCode());
			respMap.put("message", BizResponseCode.SERVICE_NOT_AVALIABLE.getMessage());
			log.info("【删除安全配置】   响应参数：{}", respMap);
			return respMap;
		 }
		 respMap.put("code", BizResponseCode.SERVICE_NOT_AVALIABLE.getCode());
		 respMap.put("message", BizResponseCode.SERVICE_NOT_AVALIABLE.getMessage());
		 log.info("【删除安全配置】   响应参数：{}", respMap);
		 return respMap;
	}
}