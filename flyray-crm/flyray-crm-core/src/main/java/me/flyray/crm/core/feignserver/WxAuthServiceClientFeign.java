package me.flyray.crm.core.feignserver;


import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import me.flyray.common.msg.BaseApiResponse;
import me.flyray.crm.core.config.WxMpConfiguration;
import me.flyray.crm.facade.feignclient.WxAuthServiceClient;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @Author: bolei
 * @date: 16:01 2019/2/11
 * @Description: 微信授权服务
 */

@Controller
public class WxAuthServiceClientFeign implements WxAuthServiceClient {

    @Override
    public BaseApiResponse<WxMpUser> wxAuth(@RequestParam("appId") String appId, @RequestParam("code") String code){

        WxMpService mpService = WxMpConfiguration.getMpServices().get(appId);
        WxMpUser user = null;
        try {
            WxMpOAuth2AccessToken accessToken = mpService.oauth2getAccessToken(code);
            user = mpService.oauth2getUserInfo(accessToken, null);
        } catch (WxErrorException e) {
            e.printStackTrace();
        }
        return BaseApiResponse.newSuccess(user);
    }

}
