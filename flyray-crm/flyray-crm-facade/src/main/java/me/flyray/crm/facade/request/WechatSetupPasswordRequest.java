package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import me.flyray.crm.facade.BaseRequest;

import javax.validation.constraints.NotNull;

/**
 * 小程序设置交易密码请求
 * @description
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "设置交易密码请求")
public class WechatSetupPasswordRequest extends BaseRequest {

	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
	
	@NotNull(message = "用户编号不能为空")
	@ApiModelProperty(value = "用户编号")
	private String customerId;
	
	@NotNull(message = "个人编号不能为空")
	@ApiModelProperty(value = "个人用户编号")
	private String personalId;
	
	@NotNull(message="设置密码不能为空")
	@ApiModelProperty(value = "设置密码")
	private String newPassword;
	
	@NotNull(message="确认设置密码不能为空")
	@ApiModelProperty(value = "确认设置密码")
	private String confirmPassword;
	
	@NotNull(message="手机号不能为空")
	@ApiModelProperty(value = "手机号")
	private String phone;
	
	@NotNull(message="手机验证码不能为空")
	@ApiModelProperty(value = "手机验证码")
	private String smsCode;

}
