package me.flyray.common.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
/**
 * 生成图形验证码
 * MobImgCodeUtil
 * @author WangCong
 * @Description
 * @date 2017年6月2日 下午7:16:31
 * @version 1.0
 */
public class MobImgCodeUtil {
	
	
	/**
	 * 生成图形验证码
	 * @return
	 */
	public static Map<String,Object> getMobImgCode(){
		Map<String,Object> map = new HashMap<String,Object>();
		// 验证码图片中可以出现的字符集，可根据需要修改
		char mapTable[] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
					'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
					'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8',
					'9' };
		int width = 100;
		int height = 30;
		BufferedImage image = new BufferedImage(width, height,BufferedImage.TYPE_INT_RGB);
		// 获取图形上下文
		Graphics g = image.getGraphics();
		// 设定背景色
		g.setColor(new Color(0xDCDCDC));
		g.fillRect(0, 0, width, height);
		// 画边框
		g.setColor(Color.black);
		g.drawRect(0, 0, width - 1, height - 1);
		// 取随机产生的认证码
		String strEnsure = "";
		// 4代表4位验证码,如果要生成更多位的认证码,则加大数值
		for (int i = 0; i < 4; ++i) {
			strEnsure += mapTable[(int) (mapTable.length * Math.random())];
		}
		// 将认证码显示到图像中,如果要生成更多位的认证码,增加drawString语句
		g.setColor(Color.black);
		g.setFont(new Font("Microsoft YaHei", Font.PLAIN, 20));
		String str = strEnsure.substring(0, 1);
		g.drawString(str, 15, 25);
		str = strEnsure.substring(1, 2);
		g.drawString(str, 35, 18);
		str = strEnsure.substring(2, 3);
		g.drawString(str, 55, 25);
		str = strEnsure.substring(3, 4);
		g.drawString(str, 75, 18);
		// 随机产生10个干扰点
		Random rand = new Random();
		for (int i = 0; i < 10; i++) {
			int x = rand.nextInt(width);
			int y = rand.nextInt(height);
			g.drawOval(x, y, 1, 1);
		}
		// 释放图形上下文
		g.dispose();
		map.put("image", image);
		map.put("strEnsure", strEnsure);
		return map;
	}

}
