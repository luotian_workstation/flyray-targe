package me.flyray.common.entity;

import lombok.Data;

import javax.persistence.Column;
import java.io.Serializable;

/**
 * @Author: bolei
 * @date: 10:34 2019/2/11
 * @Description: 类描述
 */

@Data
public class BaseIpListEntity implements Serializable {

    //平台编号
    private String platformId;

    //ip地址
    private String ip;

    //业务类型
    private String businessType;

    //状态
    private String status;

}
