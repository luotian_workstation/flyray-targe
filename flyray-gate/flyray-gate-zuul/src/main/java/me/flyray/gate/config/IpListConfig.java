package me.flyray.gate.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import me.flyray.cache.CacheProvider;
import me.flyray.common.entity.BaseIpListEntity;
import me.flyray.common.msg.BaseApiResponse;
import me.flyray.gate.feign.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * Created by xujingfeng on 2017/4/1.
 */
@Configuration
public class IpListConfig implements CommandLineRunner{
	public final static Logger logger = LoggerFactory.getLogger(IpListConfig.class);
    @Autowired
    private IUserService userService;
	@Override
	public void run(String... args) throws Exception {
		logger.info("加载白名单开始");
		List<BaseIpListEntity> redisIplist = (List<BaseIpListEntity>) CacheProvider.getList("i_flyray-admin:ipList",BaseIpListEntity.class);
    	if(null == redisIplist || redisIplist.size() < 0 ){
			BaseApiResponse<List<BaseIpListEntity>> baseIpLists = userService.getIpList();
			if (baseIpLists.getSuccess()){
				List<BaseIpListEntity> baseIpListEntities = baseIpLists.getData();
				if (baseIpListEntities.size() > 0){
					CacheProvider.set("i_flyray-admin:ipList", baseIpListEntities);
				}
			}
    	}
    	logger.info("加载白名单结束");
	}

}
