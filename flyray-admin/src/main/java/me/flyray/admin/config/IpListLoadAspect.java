package me.flyray.admin.config;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import me.flyray.admin.entity.BaseIpList;
import me.flyray.admin.entity.GatewayApiDefine;
import me.flyray.admin.mapper.BaseIpListMapper;
import me.flyray.cache.CacheProvider;
import me.flyray.common.entity.BaseIpListEntity;
import me.flyray.common.entity.GatewayApiDefineEntity;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;


/**
 * 动态记载白名单信息
 */
@Component
@Aspect
@Slf4j
@Service
public class IpListLoadAspect {
	@Autowired 
	private BaseIpListMapper maper;
	/*@Autowired 
	private CacheAPI cacheApi;*/
	@AfterReturning(value="execution(* me.flyray.admin.mapper.BaseIpListMapper.update*(..)) or "
			+ "execution(* me.flyray.admin.mapper.BaseIpListMapper.insert*(..)) or "
			+ "execution(* me.flyray.admin.mapper.BaseIpListMapper.delete*(..))"
			, argNames="rtv", returning="rtv")
	public void loadIpList(JoinPoint jp, Object rtv){
		 log.info("重新加载ip白名单开始");
		List<BaseIpListEntity> baseIpListEntities = new ArrayList<BaseIpListEntity>();
		List<BaseIpList> ipList = maper.getAllBaseIpList();
		for (int i = 0; i < ipList.size(); i++) {
			BaseIpList baseIpList = ipList.get(i);
			BaseIpListEntity baseIpListEntity = new BaseIpListEntity();
			BeanUtils.copyProperties(baseIpList,baseIpListEntity);
			baseIpListEntities.add(baseIpListEntity);
		}
		CacheProvider.set("i_flyray-admin:ipList", baseIpListEntities);
		log.info("重新加载ip白名单结束");
	}

	
}
