package me.flyray.admin.feignserver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.flyray.common.entity.BaseIpListEntity;
import me.flyray.common.msg.BaseApiResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import me.flyray.admin.biz.BaseIpListBiz;
import me.flyray.admin.entity.BaseIpList;
import me.flyray.common.msg.ResponseCode;

@Controller
@RequestMapping("feign/ip")
public class FeignIp {
	@Autowired
	private BaseIpListBiz ipBiz;
	
	/**
	 * 获取白名单信息
	 */
	@RequestMapping(value = "list",method = RequestMethod.POST)
    @ResponseBody
	public BaseApiResponse<List<BaseIpListEntity>> getIplist() {
		List<BaseIpList> ipList = ipBiz.selectListAll();
		List<BaseIpListEntity> baseIpListEntities = new ArrayList<BaseIpListEntity>();
		for (int i = 0; i < ipList.size(); i++) {
			BaseIpList baseIpList = ipList.get(i);
			BaseIpListEntity baseIpListEntity = new BaseIpListEntity();
			BeanUtils.copyProperties(baseIpList,baseIpListEntity);
			baseIpListEntities.add(baseIpListEntity);
		}
		return BaseApiResponse.newSuccess(baseIpListEntities);
	}
}
