package me.flyray.admin.rest;

import me.flyray.admin.biz.RoleBiz;
import me.flyray.admin.entity.Role;
import me.flyray.admin.vo.AuthorityMenuTree;
import me.flyray.admin.vo.GroupUsers;
import me.flyray.common.msg.BaseApiResponse;
import me.flyray.common.msg.TableResultResponse;
import me.flyray.common.rest.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 *　@author bolei
 *　@date Apr 8, 2018
　*　@description 
**/

@RestController
@RequestMapping("role")
public class RoleController extends BaseController<RoleBiz, Role> {
	
	@Autowired
	private RoleBiz roleBiz;
	
	/**
	 * 根据部门查询部门下的角色
	 * @return
	 */
	@RequestMapping(value = "/pageList", method = RequestMethod.GET)
    @ResponseBody
	public TableResultResponse<Role> queryList(@RequestParam Map<String, Object> params){
		params.put("platformId", setPlatformId((String) params.get("platformId")));
		return roleBiz.queryList(params);
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public List<Role> list(String deptId) {
        Example example = new Example(Role.class);
        example.createCriteria().andEqualTo("deptId", deptId);
        return baseBiz.selectByExample(example);
    }
		
    @RequestMapping(value = "/{id}/user", method = RequestMethod.PUT)
    @ResponseBody
    public BaseApiResponse modifiyUsers(@PathVariable int id, String members, String leaders){
        baseBiz.modifyRoleUsers(id, members, leaders);
        return new BaseApiResponse();
    }

    @RequestMapping(value = "/{id}/user", method = RequestMethod.GET)
    @ResponseBody
    public BaseApiResponse<GroupUsers> getUsers(@PathVariable int id){
        return new BaseApiResponse<GroupUsers>((GroupUsers)baseBiz.getRoleUsers(id));
    }

    @RequestMapping(value = "/{id}/authority/menu", method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse modifyMenuAuthority(@PathVariable  int id, String menuTrees){
        String [] menus = menuTrees.split(",");
        baseBiz.modifyAuthorityMenu(id, menus);
        return new BaseApiResponse();
    }

    @RequestMapping(value = "/{id}/authority/menu", method = RequestMethod.GET)
    @ResponseBody
    public BaseApiResponse<List<AuthorityMenuTree>> getMenuAuthority(@PathVariable  int id){
        return new BaseApiResponse(baseBiz.getAuthorityMenu(id));
    }

    @RequestMapping(value = "/{id}/authority/element/add", method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse addElementAuthority(@PathVariable  int id,int menuId, int elementId){
        baseBiz.modifyAuthorityElement(id,menuId,elementId);
        return new BaseApiResponse();
    }

    @RequestMapping(value = "/{id}/authority/element/remove", method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse removeElementAuthority(@PathVariable int id,int menuId, int elementId){
        baseBiz.removeAuthorityElement(id,menuId,elementId);
        return new BaseApiResponse();
    }

    @RequestMapping(value = "/{id}/authority/element", method = RequestMethod.GET)
    @ResponseBody
    public BaseApiResponse<List<Integer>> getElementAuthority(@PathVariable  int id,int menuId){
        return new BaseApiResponse(baseBiz.getAuthorityMenuElement(id,menuId));
    }

}
