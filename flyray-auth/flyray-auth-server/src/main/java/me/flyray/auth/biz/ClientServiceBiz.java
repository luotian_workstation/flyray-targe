package me.flyray.auth.biz;

import me.flyray.auth.entity.ClientService;
import me.flyray.auth.mapper.ClientServiceMapper;
import me.flyray.common.biz.BaseBiz;
import org.springframework.stereotype.Service;

/**
 * @author ace
 * @create 2017/12/30.
 */
@Service
public class ClientServiceBiz extends BaseBiz<ClientServiceMapper, ClientService> {
}
